import React from 'react'
import { Container } from 'react-bootstrap'
import './portfolio.scss'


const Portfolio = ({portfoliopan}) => {
    return (
        <div className="portfolio-holder">
            <Container>
                <ul className="portfolioPanel list-unstyled">
                    
                    {
                        portfoliopan.map((x, i) => {
                            return (
                                <li key={i} className="portfolio">
                                    <div className="portfolioImage">
                                        <img src={x.img} className="w-100" alt={x.alt} />
                                    </div>
                                    <div className="portfolioDesc">
                                        <h2>{x.title}</h2>
                                        <p>{x.para1}</p>
                                        <p>{x.para2}</p>
                                        <span className="h-line"></span>
                                    </div>
                                </li>

                            )
                        })
                    }
                </ul>
            </Container>
        </div>
    )
}

export default Portfolio