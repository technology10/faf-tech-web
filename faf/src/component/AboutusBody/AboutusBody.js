import React from 'react'
import { Container } from 'react-bootstrap'
import './AboutusBody.scss'


const AboutusBody = () => {
    return (
        <div className="aboutusBody-holder">
            <div className="whoweAre">
                <Container>
                    <h4>Who we are</h4>
                    <p>Lorem ipsum dolor sit   amet,  consectetur  adipiscing  elit,  sed  do  eiusmod  tempor  incididunt  ut  labore   et   dolore   magna   aliqua.   Ut   enim   ad  minim  veniam,  quis  nostrud  exercitation  ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure  dolor  in  reprehenderit  in  voluptate velit esse cillum dolore eu fugiat nulla pariatur.   Excepteur  sint  occaecat cupidatat  non  proident,  sunt  in  culpa  qui  officia deserunt mollit anim idest laborum. Lorem ipsum dolor sit   amet,  consectetur  adipiscing  elit,  sed  do  eiusmod  tempor  incididunt  ut  labore   et   dolore   magna   aliqua.</p>
                    <span className="h-line"></span>
                </Container>
            </div>
            <div className="gradientBanner">
                <h2>We believe in making the complex simple</h2>
                <Container>
                    <div className="bannerImg">
                        <img src="images/gradient-banner-img.png" alt="ban-img" />
                    </div>
                </Container>
            </div>
            <Container>
                <div className="ourMoto">
                    <div className="ourMoto-desc">
                        <h4>Our Vision</h4>
                        <p>Lorem ipsum dolor sit   amet,  consectetur  adipiscing  elit,  sed  do  eiusmod  tempor  incididunt  ut  labore   et   dolore   magna   aliqua.   Ut   enim   ad  minim  veniam,  quis  nostrud  exercitation  ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure  dolor  in  reprehenderit  in  voluptate velit esse cillum dolore eu fugiat nulla pariatur.   Excepteur  sint  occaecat cupidatat  non  proident,  sunt  in  culpa  qui  officia deserunt mollit anim idest laborum. Lorem ipsum dolor sit   amet,  consectetur  adipiscing  elit,  sed  do  eiusmod  tempor  incididunt  ut  labore   et   dolore   magna   aliqua.</p>
                        <p>Lorem ipsum dolor sit   amet,  consectetur  adipiscing  elit,  sed  do  eiusmod  tempor  incididunt  ut  labore   et   dolore   magna   aliqua.   Ut   enim   ad  minim  veniam,  quis  nostrud  exercitation  ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure  dolor  in  reprehenderit  in  voluptate velit esse cillum dolore eu fugiat nulla pariatur.</p>
                        <span className="h-line"></span>
                        <div className="descImgs">
                            <div className="leftImg">
                                <img src="images/desc-img-1.png" alt="descimg1" />
                            </div>
                            <div className="rightImg">
                                <img src="images/desc-img-2.png" alt="descimg2" />
                            </div>
                        </div>
                        <h4>Our Mission</h4>
                        <p>Lorem ipsum dolor sit   amet,  consectetur  adipiscing  elit,  sed  do  eiusmod  tempor  incididunt  ut  labore   et   dolore   magna   aliqua.   Ut   enim   ad  minim  veniam,  quis  nostrud  exercitation  ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure  dolor  in  reprehenderit  in  voluptate velit esse cillum dolore eu fugiat nulla pariatur.   Excepteur  sint  occaecat cupidatat  non  proident,  sunt  in  culpa  qui  officia deserunt mollit anim idest laborum. Lorem ipsum dolor sit   amet,  consectetur  adipiscing  elit,  sed  do  eiusmod  tempor  incididunt  ut  labore   et   dolore   magna   aliqua.</p>
                        <p>Lorem ipsum dolor sit   amet,  consectetur  adipiscing  elit,  sed  do  eiusmod  tempor  incididunt  ut  labore   et   dolore   magna   aliqua.   Ut   enim   ad  minim  veniam,  quis  nostrud  exercitation  ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure  dolor  in  reprehenderit  in  voluptate velit esse cillum dolore eu fugiat nulla pariatur.</p>
                        <span className="h-line"></span>
                    </div>
                    <div className="ourMoto-imgs">
                        <div>
                            <img src="images/moto-img-1.png" alt="motoimg1" />
                        </div>
                        <div>
                            <img src="images/moto-img-2.png" alt="motoimg2" />
                        </div>
                    </div>
                </div>
            </Container>
        </div>
    )
}

export default AboutusBody
