import React from 'react'
import { Container } from 'react-bootstrap'
import './ProductPanel.scss'


const ProductPanel = ({ productpanelholderpan }) => {
    return (
        <div className="productPanel-holder">
            <Container>
                {
                    productpanelholderpan.map((x, i) => {
                        return (
                            <div key={i}>
                                <div className="productPanel-text">
                                    <h2>{x.title}</h2>
                                    <p>{x.para}</p>
                                    <span className="h-line"></span>
                                </div>
                                <ul className="productPanel-block list-unstyled">
                                    {
                                        x.productpanelpan.map((y, j) => {
                                            return (
                                                <li key={j} className="productPanel">
                                                    <figure>
                                                        <img src={y.img} alt={y.alt} />
                                                    </figure>
                                                    <p className="para1">{y.para1}</p>
                                                    <p className="para2">{y.para2}</p>
                                                </li>
                                            )
                                        })
                                    }
                                </ul>
                            </div>
                        )
                    })
                }
            </Container>
        </div>
    )
}

export default ProductPanel