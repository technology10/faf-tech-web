import React from 'react'
import { Container } from 'react-bootstrap'
import Link from 'next/link'
import './contactSection.scss'



const ContactSection = () => {
  return (
    <div className="contactSection-holder text-center">
        <Container>
            <h3>Ready to get started?</h3>
            <h2><Link href="#">
              <a>Tell us about your project</a>
              </Link>
            </h2>
            <div className="allDevices-block">
              <img src="images/all-devices.png" className="w-100" alt="" />
            </div>
        </Container>
    </div>
  )
}

export default ContactSection
