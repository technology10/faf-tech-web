import React from 'react'
import './Banner.scss'
import { Container, Breadcrumb } from 'react-bootstrap'


const Banner = ({bannerpan}) => {
  return (
    <div className="banner" bannerpan={bannerpan}>
      {
        bannerpan.map((x, i) => {
          return (
            <div key={i}>
              <picture>
                <source media="(min-width:992px)" srcSet={x.imgDesk} />
                <source media="(min-width:600px)" srcSet={x.imgTab} />
                <source media="(min-width:0px)" srcSet={x.imgMob} />
                <img src={x.imgDesk} alt={x.alt} />
              </picture>
              <div className="bannerPan">
                <Container>
                  <div className="banner-text">
                    <h1>{x.title}</h1>
                  </div>
                  <div className="myBreadcrumb">
                    <Breadcrumb>
                      <Breadcrumb.Item href="/index">{x.breadParent}</Breadcrumb.Item>
                      <Breadcrumb.Item active>{x.breadActive}</Breadcrumb.Item>
                    </Breadcrumb>
                  </div>
                </Container>
              </div>
            </div>
          )
        })
      }




    </div>
  )
}

export default Banner
