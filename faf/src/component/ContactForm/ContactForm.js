import React from 'react'
import { Form, Button } from 'react-bootstrap'
import './ContactForm.scss'


const ContactForm = () => {
    return (
        <div className="contactForm-holder">
            <h2 className="text-center">Ready to Get Started?</h2>
            <Form>
                <Form.Group controlId="formBasicEmail">
                    <Form.Label>Full Name</Form.Label>
                    <Form.Control type="text" placeholder="Enter your full name" />
                </Form.Group>

                <Form.Group controlId="formBasicPassword">
                    <Form.Label>Email</Form.Label>
                    <Form.Control type="email" placeholder="Enter your email address" />
                </Form.Group>
                <Form.Group controlId="exampleForm.ControlTextarea1">
                    <Form.Label>Project Description</Form.Label>
                    <Form.Control as="textarea" rows="8" placeholder="Short project description" />
                </Form.Group>
                <Button variant="primary" type="submit" className="w-100">
                    Send Request
                </Button>
            </Form>
        </div>
    )
}

export default ContactForm