import React, { Component } from "react";
import Slider from "react-slick";
import './serviceSection.scss';
import IcomoonReact from 'icomoon-react'
import iconSet from '../../../public/fonts/icomoon/selection.json'
import { Container } from "react-bootstrap";


const ServiceSection = ({ mysliderpan }) => {
  const settings = {
    infinite: false,
    arrows: false,
    slidesToShow: 3,
    speed: 500,
    rows: 2,
    slidesPerRow: 1,
    slidesToScroll: 3,
    dots: true,
    responsive: [
      {
        breakpoint: 1200,
        settings: {
          rows: 2,
          slidesPerRow: 2,
          slidesToShow: 1,
          slidesToScroll: 1
        }
      },
      {
        breakpoint: 992,
        settings: {
          rows: 1,
          slidesPerRow: 2,
          slidesToShow: 1,
          slidesToScroll: 1
        }
      },
      {
        breakpoint: 768,
        settings: {
          rows: 1,
          slidesPerRow: 1,
          slidesToShow: 1,
          slidesToScroll: 1

        }
      }
    ]
  };
  return (
    <div className="serviceSection-holder">
      {
        mysliderpan.map((x, i) => {
          return (
            <div key={i}>
              <Container>
                <div className="serviceContent">
                  <h3>{x.subtitle}</h3>
                  <h2>{x.title}</h2>
                  <p>{x.desc}</p>
                  <span className="h-line"></span>
                </div>
                <Slider {...settings} unslick={x.unslick}>
                  {
                    x.Card.map((y, j) => {
                      return (
                        <div key={j} className="serviceCard">
                          <IcomoonReact iconSet={iconSet} size={60} icon={y.icon} />
                          <h4>{y.title}</h4>
                          <p>{y.desc}</p>
                        </div>
                      )
                    })
                  }
                </Slider>
              </Container>
              <div className="themeRectangle">
                <img src="images/Rectangle 537.png" alt="" />
              </div>
              <div className="themeRectangle left">
                <img src="images/Rectangle 537.png" alt="" />
              </div>
            </div>
          )
        })
      }
    </div>
  );
}

export default ServiceSection
