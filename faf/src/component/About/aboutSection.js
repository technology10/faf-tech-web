import React from 'react'
import { Container, Row, Col } from 'react-bootstrap'
import './aboutSection.scss'


const AboutSection = () => {
  return (
    <div className="aboutSection-holder">
      <Container>
        <Row className="align-items-center">
          <Col xs={12} md={6}>
            <div className="aboutImg">
              <img src="images/about-img.png" className="w-100" alt="" />
            </div>
          </Col>
          <Col xs={12} md={6}>
            <div className="aboutContent">
              <h3 className="m-0">About us</h3>
              <h2>Who we are</h2>
              <p>
                We are a team of creative people who are committed to giving the world a little touch of beauty with our designs. We love what we do and we do it with passions. We are a team of creative people who are committed to giving the world a little touch of beauty with our designs. We love what we do and we do it with passions.
              </p>
              <p>
                We are a team of creative people who are committed to giving the world a little touch of beauty with our designs.
              </p>
              <span className="h-line"></span>
            </div>
          </Col>
        </Row>
        <div className="polygonBlock topPolygon">
          <img src="images/polygon.png" alt="" />
        </div>
        <div className="polygonBlock bottomPolygon">
          <img src="images/polygon.png" alt="" />
        </div>
      </Container>
    </div>
  )
}

export default AboutSection
