import React from 'react'
import { Container, Button } from 'react-bootstrap'
import './getstartedStrip.scss'
import Link from 'next/link'


const GetStartedStrip = () => {
    return (
        <div className="getstartedStrip-holder">
            <Container>
                <div className="getstartedStrip-block">
                    <div className="stripText">
                        <h4>Ready to get started?</h4>
                        <h3>Tell us about your project</h3>
                    </div>
                    <div className="stripAction">
                        <Link href="/contact">
                            <a><Button variant="light">Get Started</Button></a>
                        </Link>
                    </div>
                </div>
            </Container>
        </div>
    )
}

export default GetStartedStrip
