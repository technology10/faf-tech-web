import React from 'react'
import { Container } from 'react-bootstrap'
import Link from 'next/link'
import './Footer.scss'

const Footer = () => {
  return (
    <div className="footer-holder">
      <Container>
        <div className="content">
          <div className="all-links">
            <div className="company-links">
              <p>Company</p>
              <ul className="list-unstyled">
                <Link href="#">
                  <a>About us</a>
                </Link>
                <Link href="#">
                  <a>Services</a>
                </Link>
                <Link href="#">
                  <a>Technologies</a>
                </Link>
              </ul>
            </div>
            <div className="social-links">
              <p>Follow us</p>
              <ul className="list-unstyled">
                <Link href="#">
                  <a>Facebook</a>
                </Link>
                <Link href="#">
                  <a>Instagram</a>
                </Link>
                <Link href="#">
                  <a>Twitter</a>
                </Link>
              </ul>
            </div>
            <div className="social-links">
              <p>Contact us</p>
              <ul className="list-unstyled">
                <Link href="#">
                  <a>hello@faftechnologies.com</a>
                </Link>
              </ul>
            </div>
          </div>
          <p>© 2020 - All rights reserved</p>
        </div>
      </Container>
    </div>
  )
}

export default Footer
