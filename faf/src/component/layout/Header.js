import React from 'react'
import { Nav, Navbar, Container } from 'react-bootstrap'
import Link from './ActiveLink'
import IcomoonReact from 'icomoon-react'
import iconSet from '../../../public/fonts/icomoon/selection.json'
import './Header.scss'



const Header = () => {
  return (
    <Navbar expand="lg">
      <Container>
        <Navbar.Brand href="/index">
        <IcomoonReact iconSet={iconSet} size={80} icon="faf-blue-logo" />
        <img src="images/logo.png" alt="logo" />
        </Navbar.Brand>
        <Navbar.Toggle aria-controls="basic-navbar-nav" />
        <Navbar.Collapse id="basic-navbar-nav">
          <Nav>
            <Link href="/about" activeClassName="active"><a>ABOUT US</a></Link>
            <Link href="/service" activeClassName="active"><a>SERVICES</a></Link>
            <Link href="/technology" activeClassName="active"><a>TECHNOLOGIES</a></Link>
            <Link href="/product" activeClassName="active"><a>PRODUCTS</a></Link>
            <Link href="/contact" activeClassName="active"><a>CONTACT</a></Link>
            {/* <div className="lang-switch"><span>عربي</span><IcomoonReact iconSet={iconSet} size={16} icon="globe" /></div> */}
          </Nav>
        </Navbar.Collapse>
      </Container>
    </Navbar>
  )
}

export default Header
