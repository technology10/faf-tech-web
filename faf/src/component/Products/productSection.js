
import React, { Component } from "react";
import { Container } from 'react-bootstrap'
import Slider from "react-slick";
import './productSection.scss'
import Link from 'next/link'

export default class ProductSection extends Component {
  render() {
    const settings = {
      dots: true,
      infinite: true,
      arrows: false,
      speed: 500,
      slidesToShow: 3,
      slidesToScroll: 3,
      responsive: [
        {
          breakpoint: 992,
          settings: {
            slidesToShow: 2,
            slidesToScroll: 2
          }
        },
        {
          breakpoint: 768,
          settings: {
            slidesToShow: 1,
            slidesToScroll: 1
          }
        }
      ]
    };
    return (
      <div className="productSection-holder">
        <Container>
          <h3>Products</h3>
          <h2>Solutions that power next generation business.</h2>
          <p>
            vel cu paulo doctus vidisse. Iudico dicant nostrum nec an, in eos iIn detraxit eleifend duo, alterum iudicabit consectetuer per ad. vel cu paulo doctus vidisse. Iudico dicant nostrum nec an, in eos iIn detraxit eleifend duo, alterum iudicabit consectetuer per ad.
        </p>
          <span className="h-line"></span>
          <div className="productSlider">
            <Slider {...settings}>
              <div className="productCard text-center">
                <div className="logo-holder">
                  <img src="images/Hlafly-logo.png" alt="" />
                </div>
                <p>
                  vel cu paulo doctus vidisse. Iudico dicant nostrum nec an, in eos iIn detraxit eleifend.
                </p>
                <Link href="#">
                  <a className="knowMore">Know More</a>
                </Link>
              </div>
              <div className="productCard maharahCard text-center">
                <div className="logo-holder">
                  <img src="images/Maharah-logo.png" alt="" />
                </div>
                <p>
                  vel cu paulo doctus vidisse. Iudico dicant nostrum nec an, in eos iIn detraxit eleifend.
            </p>
                <Link href="#">
                  <a className="knowMore">Know More</a>
                </Link>
              </div>
              <div className="productCard text-center">
                <div className="logo-holder">
                  <img src="images/Hlafly-logo.png" alt="" />
                </div>
                <p>
                  vel cu paulo doctus vidisse. Iudico dicant nostrum nec an, in eos iIn detraxit eleifend.
            </p>
                <Link href="#">
                  <a className="knowMore">Know More</a>
                </Link>
              </div>
              <div className="productCard maharahCard text-center">
                <div className="logo-holder">
                  <img src="images/Maharah-logo.png" alt="" />
                </div>
                <p>
                  vel cu paulo doctus vidisse. Iudico dicant nostrum nec an, in eos iIn detraxit eleifend.
            </p>
                <Link href="#">
                  <a className="knowMore">Know More</a>
                </Link>
              </div>
            </Slider>
          </div>
        </Container>
        <div className="themeRectangle">
            <img src="images/Rectangle 537.png" alt="" />
          </div>
          <div className="themeRectangle left">
            <img src="images/Rectangle 537.png" alt="" />
          </div>
      </div>
    );
  }
}

