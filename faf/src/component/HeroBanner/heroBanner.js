import React from 'react'
import './heroBanner.scss'


const HeroBanner = () => {
  return (
    <div className="heroBanner-holder text-center">
      <picture>
        <img src="images/hero-banner.png" alt="" />
      </picture>
      <div className="sticky-image-block">
        <h1>We do provide Creative IT Solution for your Business!</h1>
        <img src="images/sticky-image.png" className="w-100" alt="" />
      </div>
    </div>
  )
}

export default HeroBanner
