import React from 'react'
import IcomoonReact from 'icomoon-react'
import iconSet from '../../../public/fonts/icomoon/selection.json'
import Link from 'next/link'
import './technologySection.scss'


const TechContent = ({techpan}) => {
  return (
    <div className="technologyContent">
      {
        techpan.map((x, i) => {
          return (
            <div key={i}>
              <div className="technologyText">
                <h3>{x.subtitle}</h3>
                <h2>{x.title}</h2>
                <p>{x.para}</p>
                <span className="h-line"></span>
              </div>
              <ul className="allTechnologies list-unstyled">
                  {
                    x.softwares.map((y, j) => {
                      return (
                        <li key={j} className="text-center">
                          <Link href="#">
                            <a>
                              <IcomoonReact iconSet={iconSet} size={40} icon={y.icon} />
                              <p>{y.iconname}</p>
                            </a>
                          </Link>
                        </li>
                      )
                    })
                  }
              </ul>
            </div>
          )
        })
      }
    </div>
  )
}

export default TechContent
