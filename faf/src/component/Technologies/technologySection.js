import React from 'react'
import { Container, Row, Col } from 'react-bootstrap'
import Link from 'next/link'
import './technologySection.scss'
import TechContent from './techContent'





const TechnologySection = ({techpan}) => {
  return (
    <div className="technologySection-holder">
      <Container>
        <Row>
          <Col xs={12} md={6}>
            <div className="technologyImg">
              <img src="images/technology-img.png" className="w-100" alt="" />
            </div>
          </Col>
          <Col xs={12} md={6}>
            <TechContent techpan={techpan} />
          </Col>
        </Row>
      </Container>
      <div className="polygonBlock topPolygon">
        <img src="images/polygon.png" alt="" />
      </div>
      <div className="polygonBlock bottomPolygon">
        <img src="images/polygon.png" alt="" />
      </div>
    </div>
  )
}

export default TechnologySection
