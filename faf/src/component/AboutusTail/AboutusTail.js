import React from 'react'
import { Container } from 'react-bootstrap'
import IcomoonReact from 'icomoon-react'
import iconSet from '../../../public/fonts/icomoon/selection.json'
import GetStartedStrip from '../GetStartedStrip/getstartedStrip'
import './AboutusTail.scss'



const AboutusTail = () => {
    return (
        <div className="aboutusTail-holder">
            <Container>
                <h3 className="valueHeading">The values that hold us true and to account</h3>
                <div className="ourValues">
                    <div className="value">
                        <IcomoonReact iconSet={iconSet} size={28} icon="blue-tick" />
                        <h4>Simplicity</h4>
                        <p>Lorem ipsum dolor sit   amet,  consectetur  adipiscing  elit,  sed  do</p>
                    </div>
                    <div className="value">
                        <IcomoonReact iconSet={iconSet} size={28} icon="green-tick" />
                        <h4>Trust</h4>
                        <p>Lorem ipsum dolor sit   amet,  consectetur  adipiscing  elit,  sed  do</p>
                    </div>
                    <div className="value">
                        <IcomoonReact iconSet={iconSet} size={28} icon="orange-tick" />
                        <h4>Social good</h4>
                        <p>Lorem ipsum dolor sit   amet,  consectetur  adipiscing  elit,  sed  do</p>
                    </div>
                </div>
            </Container>
            <GetStartedStrip />
        </div>
    )
}

export default AboutusTail
