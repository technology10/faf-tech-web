import React from 'react'
import Layout from '../src/component/layout/layout'
import Banner from '../src/component/Banner/Banner'
import Portfolio from '../src/component/portfolio/portfolio'
import GetStartedStrip from '../src/component/GetStartedStrip/getstartedStrip'
import data from '../data/technologyPageData.json'
import TechContent from '../src/component/Technologies/techContent'
import { Container } from 'react-bootstrap'



const bannerpanData = data.banner;
const portfoliopanData = data.portfolio;
const techpanData = data.technology;


const Technology = () => {
  return (
    <main className="TechnologyPage">
      <Layout titleTag="Technology">
        <Banner bannerpan={bannerpanData} />
        <section className="techPage">
          <Portfolio portfoliopan={portfoliopanData} />
          <Container>
            <TechContent techpan={techpanData} />
          </Container>
          <GetStartedStrip />
        </section>
      </Layout>
    </main>
  )
}

export default Technology
