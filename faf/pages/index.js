import React from 'react'
import Layout from '../src/component/layout/layout'
import AboutSection from '../src/component/About/aboutSection'
import TechnologySection from '../src/component/Technologies/technologySection'
import ProductSection from '../src/component/Products/productSection'
import ContactSection from '../src/component/Contact/contactSection'
import HeroBanner from '../src/component/HeroBanner/heroBanner'
import ServiceSection from '../src/component/Services/serviceSection'
import data from '../data/homePageData.json'


const mysliderpanData = data.service;
const techpanData = data.technology;


const Home = () => {
  return (
    <main className="HomePage">
      <Layout titleTag="Home">
        <HeroBanner />
        <AboutSection />
        <ServiceSection mysliderpan={mysliderpanData}/>
        <TechnologySection techpan={techpanData} />
        <ProductSection />
        <ContactSection />
      </Layout>
    </main>
  )
}

export default Home
