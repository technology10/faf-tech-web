import React from 'react'
import Layout from '../src/component/layout/layout'
import Banner from '../src/component/Banner/Banner'
import Portfolio from '../src/component/portfolio/portfolio'
import GetStartedStrip from '../src/component/GetStartedStrip/getstartedStrip'
import data from '../data/servicePageData.json'
import ServiceSection from '../src/component/Services/serviceSection'


const bannerpanData = data.banner;
const portfoliopanData = data.portfolio;
const mysliderpanData = data.service;

const Service = () => {
  return (
    <main className="ServicePage">
      <Layout titleTag="Service">
        <Banner bannerpan={bannerpanData} />
        <section>
          <Portfolio portfoliopan={portfoliopanData} />
          <ServiceSection mysliderpan={mysliderpanData} />
          <GetStartedStrip />
        </section>
      </Layout>
    </main>
  )
}

export default Service
