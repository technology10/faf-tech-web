import React from 'react'
import Layout from '../src/component/layout/layout'
import Banner from '../src/component/Banner/Banner'
import data from '../data/aboutPageData.json'
import AboutusBody from '../src/component/AboutusBody/AboutusBody'
import AboutusTail from '../src/component/AboutusTail/AboutusTail'


const bannerpanData = data.banner;

const About = () => {
  return (
    <main className="AboutPage">
      <Layout titleTag="About">
        <Banner bannerpan={bannerpanData} />
        <section>
          <AboutusBody />
          <AboutusTail />
        </section>
      </Layout>
    </main>
  )
}

export default About
