import React from 'react'
import { Container } from 'react-bootstrap'
import Layout from '../src/component/layout/layout'
import Banner from '../src/component/Banner/Banner';
import data from '../data/contactPageData.json'



const bannerpanData = data.banner;


const Contact = () => {
  return (
    <main className="ContactPage">
      <Layout titleTag="contact">
        <Banner bannerpan={bannerpanData} />
        <section>
          <Container>
            <div className="head text-center">
              <h2>Contact Us</h2>
              <p>Give us a call or drop by anytime, we endeavour to answer all enquiries within 24 hours on business days. We will be happy to answer your questions.</p>
              <span className="h-line"></span>
            </div>
            <div className="text-center mt-5">
              <p><b>Email:</b> ahmedkhan@hlafly.com</p>
              <p><b>Phone:</b> +966540234418</p>
            </div>
          </Container>
        </section>
      </Layout>
    </main>
  )
}

export default Contact
