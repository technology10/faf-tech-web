import React from 'react'
import Layout from '../src/component/layout/layout'
import Banner from '../src/component/Banner/Banner'
import Portfolio from '../src/component/portfolio/portfolio'
import ProductPanel from '../src/component/ProductPanel/ProductPanel'
import GetStartedStrip from '../src/component/GetStartedStrip/getstartedStrip'
import data from '../data/productPageData.json'



const bannerpanData = data.banner;
const portfoliopanData = data.portfolio;
const productpanelholderpanData = data.productpanelholder;


const Product = () => {
  return (
    <main className="ProductPage">
      <Layout titleTag="Product">
        <Banner bannerpan={bannerpanData} />
        <section>
          <Portfolio portfoliopan={portfoliopanData} />
          <ProductPanel productpanelholderpan={productpanelholderpanData} />
          <GetStartedStrip />
        </section>
      </Layout>
    </main>
  )
}

export default Product
